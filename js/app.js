$(document).foundation();

$('.ui.dropdown').dropdown({
     action : 'select'
});

$('.ui.dropdown').dropdown('setting', 'onChange', function(val){

   $(this).find('.selection').text(val);
   $(this).css('margin-bottom', '50px');

});

var $a = $('#jsImagesHook');

$('#jsImagesHook').on('click', 'a', function(){

   var b = $(this).find('img').attr('src');

   if($('.opaque').length == 1) {

      $('.opaque').removeClass('opaque');
      $(this).addClass('opaque');

   } else {

      $(this).addClass('opaque');

   }

   $a.find('.watchinfo__images--main > img').attr('src', b);

});


$('#carousel').slick({
  centerMode: true,
  centerPadding: '150px',
  slidesToShow: 1,
  prevArrow: '<div class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></div>',
  nextArrow: '<div class="slick-next slick-nav"><i class="fa fa-angle-right"></i></div>',
  dots: true,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        centerMode: true,
        centerPadding: '80px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});


$('#carousel_object').slick({
  centerMode: true,
  centerPadding: '150px',
  slidesToShow: 2,
  prevArrow: '<div class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></div>',
  nextArrow: '<div class="slick-next slick-nav"><i class="fa fa-angle-right"></i></div>',
  dots: true,
  responsive: [
    {
      breakpoint: 850,
      settings: {
        centerMode: true,
        centerPadding: '30px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 550,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
